# nilvana inference-runtime recognition node

## Get Started

```shell
npm install --save  https://gitlab.com/nilvana-ai/edge/nvir-recognition-node.git
```

## Remove node
```
npm remove 11-nilvana-inference-runtime-recognition
```

## Copyright and license

Copyright inwinSTACK Inc. under [the Apache 2.0 license](LICENSE.md).
